from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.process import pm

from memory import player_base_address


update_player_base_pointer = AllocatingCodeInjection(
    pm.base_address + 0x6D83CC,
    f"""
        mov [{player_base_address.base_address}], eax
        call dword [edx + 0x234]
    """,
    original_code_length=6,
)

infinite_items = AllocatingCodeInjection(
    pm.base_address + 0x57F769,
    """
        mov eax, [ebp + 0x10]

        test eax, eax
        jns  skip
        xor eax, eax
        mov [ebp + 0x10], eax

        skip:
        lea ecx, [edi + ecx + 0x30]
    """,
    original_code_length=7,
)

disable_collision_check = CodeInjection(pm.base_address + 0x5E4901, b"\x75")
