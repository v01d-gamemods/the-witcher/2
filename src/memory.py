from trainerbase.memory import Address, allocate
from trainerbase.process import pm


player_base_address = Address(allocate())
player_coords_address = Address(pm.base_address + 0x22AFEFC, [0x8, 0x20, 0x18, 0xAC, 0x1AC, 0x20])
