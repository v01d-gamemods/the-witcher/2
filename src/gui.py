from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI

from injections import disable_collision_check, infinite_items
from objects import adrenalin, hp, instant_kill_chance, stamina, talent_points, toxicity, weight_limit, xp
from teleport import tp


@simple_trainerbase_menu("The Witcher 2", 730, 400)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                GameObjectUI(hp, "HP", "F2", "F1", default_setter_input_value=5000.0),
                GameObjectUI(xp, "XP"),
                GameObjectUI(talent_points, "Talent Points"),
                GameObjectUI(toxicity, "Toxicity", set_hotkey="F3"),
                GameObjectUI(adrenalin, "Adrenalin"),
                GameObjectUI(weight_limit, "Weight Limit", default_setter_input_value=5000.0),
                GameObjectUI(stamina, "Stamina", "Shift+F4", "F4", default_setter_input_value=5.0),
                GameObjectUI(instant_kill_chance, "Instant Kill Chance"),
                SeparatorUI(),
                CodeInjectionUI(infinite_items, "Infinite Items", "F6"),
                CodeInjectionUI(disable_collision_check, "(Experimental) Disable Collision Check", "F7"),
            )

        with dpg.tab(label="Teleport & Speedhack"):
            add_components(
                TeleportUI(tp),
                SeparatorUI(),
                SpeedHackUI(),
            )
