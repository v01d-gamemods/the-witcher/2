from trainerbase.gameobject import GameFloat, GameInt

from memory import player_base_address, player_coords_address


talent_points = GameInt(player_base_address + [0x14, 0x2B0])
xp = GameInt(player_base_address + [0x14, 0x2AC])
hp = GameFloat(player_base_address + [0x14, 0x8])
max_hp = GameFloat(player_base_address + [0x14, 0xC])
toxicity = GameFloat(player_base_address + [0x14, 0x2A4])
adrenalin = GameFloat(player_base_address + [0x14, 0x2A8])
weight_limit = GameFloat(player_base_address + [0xC4, 0x0, 0x1C, 0x50, 0x2C])
instant_kill_chance = GameFloat(player_base_address + [0xC4, 0, 0x1C, 0x50, 0x298])
stamina = GameFloat(player_base_address + [0x14, 0x10])
max_stamina = GameFloat(player_base_address + [0x14, 0x14])

player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address + 0x4)
player_z = GameFloat(player_coords_address + 0x8)
